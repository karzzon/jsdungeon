/******************************************************************************
 ********************************* Map.js *************************************
 ******************************************************************************
 *                                                                            *
 * Copyright (C) 2013 RevertedSoft <http://revertedsoft.com/>                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation file (the "Software"),  *
 * to deal with the Software in the rights to use, copy, modify, merge        *
 * publish, distribute, but NOT to sell copies of the Software, subject to    *
 * the following conditions:                                                  *
 *                                                                            *
 * The above copyright notice, this permission notice, and the following      *
 * disclaimer shall be included in all copies or substantial portions of the  * 
 * Software.                                                                  *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *
 * DEALINGS IN THE SOFTWARE.                                                  *
 *                                                                            *
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

/*
 * Map:
 * an object that represents a 2D grid of pixels stored in an 2D array.
 *
 * Parameters:
 * width  - width of the grid of pixels.
 * height - height of the grid of pixels.
 */
function Map(width, height) {
    "use strict";
    
    var board = [];
    var down = {};
	
	this.getTile = function(location) {
        return board[location.x][location.y];
    };
    this.setTile = function(location, color) {
        board[location.x][location.y] = color;
    };
    
    this.generateBoard = function() {
        for(var x = 0; x < width; x++) {
            board.push([]);
        }
    
        for(x = 0; x < width; x++) {
            for(var y = 0; y < height; y++) {
                if(x === 0 || y === 0 || x === 23 || y === 23) {
                    board[y].push("#000");
                } else if(Math.floor((Math.random() * 100)) < 69) {
                    board[y].push("#fff");
                } else {
                    board[y].push("#000");
                }
            }
        }
    
        down = {
            x: (Math.floor(Math.random() * (20 - 3 + 1)) + 3),
            y: (Math.floor(Math.random() * (20 - 3 + 1)) + 3)
        };
    
        board[down.x][down.y] = "#ff0000";
    };
    
    this.printBoard = function(canvas, player) {
        for(var x = 0; x < width; x++) {
            for(var y = 0; y < height; y++) {
                canvas.drawRect({
                    fillStyle: board[x][y],
                    x: x, y: y,
                    width: 1,
                    height: 1,
                    fromCenter: false
                });
            }
        }
            
        canvas.drawRect({
            fillStyle: "#0000ff",
            x: player.getLocation().x, y: player.getLocation().y,
            width: 1,
            height: 1,
            fromCenter: false
        });
    };
}
