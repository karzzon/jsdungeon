/******************************************************************************
 ********************************* Player.js **********************************
 ******************************************************************************
 *                                                                            *
 * Copyright (C) 2013 RevertedSoft <http://revertedsoft.com/>                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation file (the "Software"),  *
 * to deal with the Software in the rights to use, copy, modify, merge        *
 * publish, distribute, but NOT to sell copies of the Software, subject to    *
 * the following conditions:                                                  *
 *                                                                            *
 * The above copyright notice, this permission notice, and the following      *
 * disclaimer shall be included in all copies or substantial portions of the  * 
 * Software.                                                                  *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *
 * DEALINGS IN THE SOFTWARE.                                                  *
 *                                                                            *
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/
 
function Player() {
	"use strict";
	
	var location = {
		x: 0,
		y: 0
	};
	
	this.getLocation = function() {
		return location;
	};
	this.setLocation = function(newLocation) {
		location = newLocation;
	};
	
	this.setRandomLocation = function() {
		location = {
			x: (Math.floor(Math.random() * (20 - 5 + 1)) + 5),
			y: (Math.floor(Math.random() * (20 - 5 + 1)) + 5)
		};
	};
	
	this.safeSpawn = function(floor) {
		floor.setTile({
			x: location.x,
			y: location.y
		}, "#fff");
		floor.setTile({
			x: location.x + 1,
			y: location.y
		}, "#fff");
		floor.setTile({
			x: location.x + 1,
			y: location.y + 1
		}, "#fff");
		floor.setTile({
			x: location.x,
			y: location.y + 1
		}, "#fff");
		floor.setTile({
			x: location.x - 1,
			y: location.y + 1
		}, "#fff");
		floor.setTile({
			x: location.x - 1,
			y: location.y
		}, "#fff");
		floor.setTile({
			x: location.x - 1,
			y: location.y - 1
		}, "#fff");
		floor.setTile({
			x: location.x,
			y: location.y - 1
		}, "#fff");
		floor.setTile({
			x: location.x + 1,
			y: location.y - 1
		}, "#fff");
	};
}
