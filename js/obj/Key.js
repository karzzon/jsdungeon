/******************************************************************************
 ************************************ Key.js **********************************
 ******************************************************************************
 *                                                                            *
 * Copyright (C) 2013 RevertedSoft <http://revertedsoft.com/>                 *
 *                                                                            *
 * Permission is hereby granted, free of charge, to any person obtaining a    *
 * copy of this software and associated documentation file (the "Software"),  *
 * to deal with the Software in the rights to use, copy, modify, merge        *
 * publish, distribute, but NOT to sell copies of the Software, subject to    *
 * the following conditions:                                                  *
 *                                                                            *
 * The above copyright notice, this permission notice, and the following      *
 * disclaimer shall be included in all copies or substantial portions of the  *
 * Software.                                                                  *
 *                                                                            *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   *
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    *
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    *
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *
 * DEALINGS IN THE SOFTWARE.                                                  *
 *                                                                            *
 ******************************************************************************
 ******************************************************************************
 ******************************************************************************/

var Key = {
	en_US: {
		a: 65,
		b: 66,
		c: 67,
		d: 68,
		e: 69,
		f: 70,
		g: 71,
		h: 72,
		i: 73,
		j: 74,
		k: 75,
		l: 76,
		m: 77,
		n: 78,
		o: 79,
		p: 80,
		q: 81,
		r: 82,
		s: 83,
		t: 84,
		u: 85,
		v: 86,
		w: 87,
		x: 88,
		y: 89,
		z: 90
	}
};